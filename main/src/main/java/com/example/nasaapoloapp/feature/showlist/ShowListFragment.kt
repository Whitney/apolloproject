package com.example.nasaapoloapp.feature.showlist

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.example.nasaapoloapp.R
import com.example.nasaapoloapp.model.ItemModel
import com.example.nasaapoloapp.model.LinkModel
import com.example.nasaapoloapp.utils.Constants.DEFAULT_IMAGE_LINK
import com.example.nasaapoloapp.utils.Constants.ERROR_TEXT
import com.example.nasaapoloapp.utils.getViewModel
import kotlinx.android.synthetic.main.fragment_show_list.*
import kotlinx.android.synthetic.main.layout_list_shimmer.*

class ShowListFragment : Fragment() {

    private lateinit var showListViewModel: ShowListViewModel
    private lateinit var showListAdapter: ShowListAdapter
    private var isFavorite = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_show_list, container, false)
        setViewModelConfigurations()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setFilterConfigurations()
    }

    private fun setViewModelConfigurations() {
        showListViewModel = requireActivity().getViewModel { ShowListViewModel() }
        showListViewModel.response.observe(viewLifecycleOwner, Observer(::updateUI))
    }

    private fun updateUI(model: ApolloInformationModel) {
        if (model is ApolloInformationModel.Loading) showProgress() else hideProgress()
        when (model) {
            is ApolloInformationModel.Success -> showItemList(model.list!!)
            is ApolloInformationModel.Error -> showToast(ERROR_TEXT)
        }
    }

    private fun showItemList(listItemModel: List<ItemModel>) {
        hideProgress()
        val arrayList = listItemModel as ArrayList
        showListAdapter = ShowListAdapter(
            arrayList,
            { count, position -> onItemClicked(count, position) },
            { isFavorite -> setToggleFavoriteConfiguration(isFavorite) })
        listItemRecyclerView.adapter = showListAdapter
    }

    private fun setFilterConfigurations() {
        itemSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                showListAdapter.filter.filter(newText)
                return false
            }
        })
    }

    private fun setToggleFavoriteConfiguration(isFavorite: Boolean) {
        if (isFavorite) {
            this.isFavorite = true
            showToast(getString(R.string.added_favorites))
        } else {
            this.isFavorite = false
            showToast(getString(R.string.deleted_favorites))
        }
    }

    private fun onItemClicked(option: ItemModel, position: Int) {
        val action = ShowListFragmentDirections.actionShowListFragmentToShowDetailFragment(
            isFavorite, position, option.data?.get(0)?.title!!, showPicture(option.links!!)
        )
        Navigation.findNavController(requireView()).navigate(action)
    }

    private fun showPicture(links: List<LinkModel>): String =
        if (links.isEmpty()) {
            DEFAULT_IMAGE_LINK
        } else {
            links[0].href!!
        }

    private fun showToast(text: String) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    private fun showProgress() {
        shimmerFrameLayout.startShimmer()
        shimmerFrameLayout.visibility = View.VISIBLE
        listItemRecyclerView.visibility = View.INVISIBLE
        activity?.window?.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
    }

    private fun hideProgress() {
        shimmerFrameLayout.stopShimmer()
        listItemShimmerInclude.visibility = View.GONE
        listItemRecyclerView.visibility = View.VISIBLE
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

}
