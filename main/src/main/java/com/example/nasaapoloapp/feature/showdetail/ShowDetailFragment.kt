package com.example.nasaapoloapp.feature.showdetail

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.navArgs

import com.example.nasaapoloapp.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_show_detail.*

class ShowDetailFragment : Fragment() {

    lateinit var sharedPreferences: SharedPreferences
    lateinit var editPreferences: SharedPreferences.Editor
    private val args: ShowDetailFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPreferences = requireActivity().getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)
        editPreferences = sharedPreferences.edit()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_show_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Picasso.get().load(args.image).into(itemImageDetailImageView)
        itemTitleDetailTextView.text = args.title
        favoriteDetailToggleButton.isChecked = args.isFavorite
        changeToggleFavorite()
    }

    private fun changeToggleFavorite() {
        favoriteDetailToggleButton.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                showToast(getString(R.string.added_favorites))
            } else {
                showToast(getString(R.string.deleted_favorites))
            }
        }
    }

    private fun showToast(text: String) {
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

}