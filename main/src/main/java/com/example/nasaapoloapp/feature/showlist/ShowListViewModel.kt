package com.example.nasaapoloapp.feature.showlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.nasaapoloapp.networking.ApolloInformationApi
import kotlinx.coroutines.*
import java.lang.Exception

class ShowListViewModel : ViewModel() {

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val _response = MutableLiveData<ApolloInformationModel>()
    val response: LiveData<ApolloInformationModel>
        get() = _response

    init {
        getData()
    }

    private fun getData() {
        coroutineScope.launch {
            val getApolloInformation =
                ApolloInformationApi.getApolloInformationService.getApolloInformation()
            _response.value = ApolloInformationModel.Loading
            try {
                val listItems = getApolloInformation.await()
                _response.value = ApolloInformationModel.Success(listItems.collection?.items)
            } catch (e: Exception) {
                _response.value = ApolloInformationModel.Error
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}