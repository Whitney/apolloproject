package com.example.nasaapoloapp.feature.showlist

import com.example.nasaapoloapp.model.ItemModel


sealed class ApolloInformationModel {
    //Get Card
    object Loading : ApolloInformationModel()

    data class Success(val list: List<ItemModel>?) : ApolloInformationModel()

    object Error : ApolloInformationModel()
}