package com.example.nasaapoloapp.feature.showlist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.example.nasaapoloapp.R
import com.example.nasaapoloapp.model.ItemModel
import com.example.nasaapoloapp.utils.Constants.DEFAULT_IMAGE_LINK
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_item_list.view.*
import java.util.*
import kotlin.collections.ArrayList

class ShowListAdapter(
    private val list: ArrayList<ItemModel>,
    private val listener: (option: ItemModel, position: Int) -> Unit,
    private val onClickFavorite: (isChecked: Boolean) -> Unit
) : RecyclerView.Adapter<ShowListAdapter.ListItemViewHolder>(), Filterable {

    private var filterList = arrayListOf<ItemModel>()
    private var isFavorite = false

    init {
        filterList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListItemViewHolder =
        ListItemViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_item_list, parent, false)
        )

    override fun getItemCount(): Int = filterList.size

    override fun onBindViewHolder(holder: ListItemViewHolder, position: Int) {
        val item = filterList[position]
        holder.bindView(item)
        holder.itemView.setOnClickListener {
            listener.invoke(item, position)
        }
    }

    inner class ListItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(item: ItemModel) {
            with(item) {
                itemView.itemTitleTextView.text = data?.get(0)?.title
                if (links!!.isEmpty()) {
                    Picasso.get().load(DEFAULT_IMAGE_LINK).into(itemView.itemImageView)
                } else {
                    Picasso.get().load(links[0].href).into(itemView.itemImageView)
                }
                itemView.favoriteToggleButton.setOnCheckedChangeListener { _, isChecked ->
                    isFavorite = isChecked
                    onClickFavorite(isFavorite)
                }
            }
        }
    }

    override fun getFilter(): Filter = object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val charSearch = constraint.toString()
            filterList = if (charSearch.isEmpty()) {
                list
            } else {
                val resultList = arrayListOf<ItemModel>()
                for (row in list) {
                    if (row.data?.get(0)?.title!!.trim().toLowerCase(Locale.getDefault()).contains(
                            charSearch.trim().toLowerCase(Locale.getDefault())
                        )
                    ) {
                        resultList.add(row)
                    }
                }
                resultList
            }
            return FilterResults().apply {
                values = filterList
            }
        }

        @Suppress("UNCHECKED_CAST")
        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            filterList = results?.values as ArrayList<ItemModel>
            notifyDataSetChanged()
        }
    }

}