package com.example.nasaapoloapp.model

data class ItemModel(
    val data: List<DataModel>? = listOf(),
    val links: List<LinkModel>? = listOf(),
    val href: String? = ""
)