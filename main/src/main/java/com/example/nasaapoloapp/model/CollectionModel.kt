package com.example.nasaapoloapp.model

data class CollectionModel(
    val metadata: MetadataModel? = null,
    val version: String? = "",
    val items: List<ItemModel>? = listOf(),
    val href: String? = null,
    val links: List<LinkModel>? = listOf()
)