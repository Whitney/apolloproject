package com.example.nasaapoloapp.model

import com.squareup.moshi.Json

data class MetadataModel(@Json(name = "total_hits") val totalHits: String? = "")
