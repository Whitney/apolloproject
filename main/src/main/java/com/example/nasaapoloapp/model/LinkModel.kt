package com.example.nasaapoloapp.model

data class LinkModel (
    val render: String? = "",
    val prompt: String? = "",
    val rel: String? = "",
    val href: String? = ""
)