package com.example.nasaapoloapp.model

import com.squareup.moshi.Json

data class DataModel(
    @Json(name = "date_created") val dateCreated: String? = "",
    val center: String? = "",
    val title: String? = "",
    @Json(name = "media_type") val mediaType: String? = "",
    @Json(name = "nasa_id") val nasaId: String? = "",
    val keywords: List<String>? = listOf(),
    val description: String? = ""
)
