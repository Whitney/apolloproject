package com.example.nasaapoloapp.networking

object ApolloInformationApi {

    val getApolloInformationService: ApolloInformationService by lazy {
        retrofit.create(ApolloInformationService::class.java)
    }

}