package com.example.nasaapoloapp.networking

import com.example.nasaapoloapp.model.ApolloInformationModel
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface ApolloInformationService {

    @GET("/search?q=apollo%2011")
    fun getApolloInformation(): Deferred<ApolloInformationModel>

}