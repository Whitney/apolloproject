package com.example.nasaapoloapp.utils

object Constants {

    const val DEFAULT_IMAGE_LINK = "https://hbr.org/resources/images/article_assets/2018/04/apr18-20-nasa-apollo-proj-archive-01.jpg"
    const val ERROR_TEXT = "Ha ocurrido un error"

}